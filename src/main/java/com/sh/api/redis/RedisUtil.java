package com.sh.api.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 类名：RedisUtil
 * 类说明：redis帮助类
 * Copyright: Copyright (c) 2012-2020
 * Company: HT
 *
 * @author haoxiuzhu
 * @version 1.0
 * @date 2020/6/8
 */
@Component
public class RedisUtil<T> {

    @Autowired
    private RedisTemplate<String, T> redisTemplate;

    /**
     *方法:expire
     *方法说明: * 指定缓存失效时间
     *@author     haoxiuzhu
     *@date       2020/6/8
     * @param key: 键
     * @param time: 时间(秒) 大于0
     *@retrun     * @return: boolean
     */
    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     *方法:getExpire
     *方法说明: 获取过期时间
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key:键 不能为null
     *@retrun     * @return: long 返回-1代表为永久有效
     */
    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }
    /**
     *方法:hasKey
     *方法说明:判断key是否存在
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 键
     *@retrun     * @return: boolean true 存在 false不存在
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     *方法:del
     *方法说明: 删除缓存
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 可以传一个值 或多个
     *@retrun     * @return: void
     */
    public Boolean del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                return redisTemplate.delete(key[0]);
            } else {
               return (redisTemplate.delete(CollectionUtils.arrayToList(key))>0?true:false);
            }
        }
        return false;
    }
    //================================String=================================
    /**
     *方法:getString
     *方法说明: 获取redis 字符串值
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     *@retrun     * @return: java.lang.String
     */
    public T getString(String key) {
        return key == null ? null : (T) redisTemplate.opsForValue().get(key);
    }
    /**
     *方法:setString
     *方法说明: 缓存redis 字符串
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param value: 
     *@retrun     * @return: boolean
     */
    public boolean setString(String key, T value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     *方法:setString
     *方法说明:缓存redis 字符串并设置超时时间
     *@author     haoxiuzhu
     *@date       2020/6/8
     * @param key: 键
     * @param value: 值
     * @param time: 时间(秒) time要大于0 如果time小于等于0 将设置无限期
     *@retrun     * @return: boolean
     */
    public boolean setString(String key, T value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                setString(key, value);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    //================================Map=================================
    /**
     *方法:hashGet
     *方法说明: 获取hash key对应的项键值
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param item: 
     *@retrun     * @return: java.lang.Object
     */
    public T hashGet(String key,String item){
        return (T) redisTemplate.opsForHash().get(key, item);
    }
    /**
     *方法:hashGet
     *方法说明: 获取hash Key对应的所有键值
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     *@retrun     * @return: java.util.Map<java.lang.Object,java.lang.Object>
     */
    public Map<Object,T> hashGet(String key){
        return (Map<Object, T>) redisTemplate.opsForHash().entries(key);
    }
    /**
     *方法:hashSet
     *方法说明: 设置hash key键值
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key:key键
     * @param map: 对应多个键值
     *@retrun     * @return: boolean
     */
    public boolean hashSet(String key, Map<String,T> map){
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * hashSet 设置hash key键值并设置时间
     * @param key 键
     * @param map 对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public boolean hashSet(String key, Map<String,T> map, long time){
        if(hashSet(key,map)){
            if(time>0){
                expire(key, time);
            }
            return true;
        }else {
            return  false;
        }
    }
    /**
     *方法:hashSet
     *方法说明:设置hash key键 item值;如果不存在将创建
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key:
     * @param item:
     * @param value:
     *@retrun     * @return: boolean
     */
    public boolean hashSet(String key,String item,T value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     *方法:hashSet
     *方法说明: 设置hash key键 item值并设置时间
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
    * @param item:
    * @param value:
    * @param time:
     *@retrun     * @return: boolean
     */
    public boolean hashSet(String key,String item,T value,long time) {
        if(hashSet(key, item, value)){
            if(time>0){
                expire(key, time);
            }
            return true;
        }else {
            return false;
        }
    }
    /**
     *方法:hashDel
     *方法说明: 删除hash表中的值
     *@author     haoxiuzhu
     *@date       2020/6/8
     * @param key: 键 不能为null
     * @param item: 项 可以使多个 不能为null
     *@retrun     * @return: Boolean
     */
    public Boolean hashDel(String key, String... item){
        return redisTemplate.opsForHash().delete(key,item)>0?true:false;
    }
    /**
     *方法:hashHasKey
     *方法说明: 判断hash表中是否有该项的值
     *@author     haoxiuzhu
     *@date       2020/6/8
     * @param key:
     * @param item:
     *@retrun     * @return: boolean
     */
    public boolean hashHasKey(String key, String item){
        return redisTemplate.opsForHash().hasKey(key, item);
    }
    //============================list=================================
    /**
     *方法:listGet
     *方法说明: 获取list缓存的内容 0 到 -1代表所有值
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param start: 
     * @param end: 
     *@retrun     * @return: java.util.List<java.lang.Object>
     */
    public List<T> listGet(String key, long start, long end){
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     *方法:listGet
     *方法说明: 获取list缓存的内容
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     *@retrun     * @return: java.util.List<java.lang.Object>
     */
    public List<T> listGet(String key){
        return listGet(key,0,-1);
    }
    /**
     *方法:listGetSize
     *方法说明: 获取list缓存的长度
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     *@retrun     * @return: long
     */
    public long listGetSize(String key){
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    /**
     *方法:listGetIndex
     *方法说明: 通过索引 获取list中的
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param index:  索引  index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     *@retrun     * @return: java.lang.Object
     */
    public T listGetIndex(String key,long index){
        try {
            return (T) redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     *方法:listSet
     *方法说明: 将list 项放入缓存
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param value: 
     *@retrun     * @return: boolean
     */
    public boolean listSet(String key, T value) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     *方法:listSet
     *方法说明: 将list 项放入缓存并设置超时时间(秒)
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param value: 
     * @param time: 
     *@retrun     * @return: boolean
     */
    public boolean listSet(String key, T value, long time) {
        if(listSet(key,value)){
            if (time > 0) {
                expire(key, time);
            }
            return true;
        }else {
            return  false;
        }
    }
    /**
     *方法:listSet
     *方法说明: 将list 放入缓存
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param value: 
     *@retrun     * @return: boolean
     */
    public boolean listSet(String key, List<T> value) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     *方法:listSet
     *方法说明: 将list放入缓存并设置超时时间(秒)
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param value: 
     * @param time: 
     *@retrun     * @return: boolean
     */
    public boolean listSet(String key, List<T> value, long time) {
        if(listSet(key,value)){
            if (time > 0) {
                expire(key, time);
            }
            return true;
        }else{
            return false;
        }
    }
    /**
     *方法:listUpdateIndex
     *方法说明: 根据索引修改list中的某条数据
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param index: 
     * @param value: 
     *@retrun     * @return: boolean
     */
    public boolean listUpdateIndex(String key, long index,T value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     *方法:listRemove
     *方法说明: 移除N个值为value的缓存
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param count: 
     * @param value: 
     *@retrun     * @return: long
     */
    public long listRemove(String key,long count,T value) {
        try {
            return redisTemplate.opsForList().remove(key, count, value);
        } catch (Exception e) {
            e.printStackTrace();
            return 0L;
        }
    }
    //============================set==================================
    /**
     *方法:setGet
     *方法说明: 根据key获取Set中的所有值
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     *@retrun     * @return: java.util.Set<java.lang.Object>
     */
    public Set<T> setGet(String key){
        try {
            return (Set<T>) redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     *方法:setHasKey
     *方法说明:根据value从一个set中查询,是否存在
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key:
     * @param value:
     *@retrun     * @return: boolean
     */
    public boolean setHasKey(String key,T value){
        try {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     *方法:setSet
     *方法说明: 将set数据放入缓存
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param values: 
     *@retrun     * @return: long
     */
    public long setSet(String key, T...values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    /**
     *方法:setSet
     *方法说明: 将set数据放入缓存并设置超时时间(秒)
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param time: 
     * @param values: 
     *@retrun     * @return: long
     */
    public long setSet(String key,long time,T...values) {
        Long count = setSet(key, values);
        if(time>0) {
            expire(key, time);
        }
        return count;
    }
    /**
     *方法:setGetSize
     *方法说明: 获取set缓存的长度
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     *@retrun     * @return: long
     */
    public long setGetSize(String key){
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    /**
     *方法:setRemove
     *方法说明: 移除set中值为value的
     *@author     haoxiuzhu
     *@date       2020/6/8
     *@Param      * @param key: 
     * @param values: 
     *@retrun     * @return: long
     */
    public long setRemove(String key, T ...values) {
        try {
            Long count = redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
